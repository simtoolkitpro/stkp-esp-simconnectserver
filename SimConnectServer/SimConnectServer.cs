﻿using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SCServer
{
    public partial class SimConnectServer : Form
    {
        SimConnect simConnectService = null;
        const int WM_USER_SIMCONNECT = 0x0402;

        enum DEFINITIONS
        {
            Struct1,
        }

        enum DATA_REQUESTS
        {
            REQUEST_1,
        };

        // this is how you declare a data structure so that
        // simconnect knows how to fill it/read it.
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        struct Struct1
        {
            // this is how you declare a fixed size string
            public int paused;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public String title;
            public double latitude;
            public double longitude;
            public double altitudemsl;
            public double fuel_total_weight;
            public double altitudeagl;
            public double ac_pitch;
            public double ac_bank;
            public double magvar;
            public double airspeed_true;
            public double airspeed_indicated;
            public double vspeed;
            public double heading;
            public double gforce;
            public double eng1_rpm;
            public double eng2_rpm;
            public double eng3_rpm;
            public double eng4_rpm;
            public double eng5_rpm;
            public double eng6_rpm;
            public int zulu_time;
            public int local_time;
            public int on_ground;


        };

        public SimConnectServer()
        {
            InitializeComponent();
            SimConnectConnect();
        }

        private void SimConnectConnect()
        {
            try
            {
                simConnectService = new SimConnect("Managed Data Request", this.Handle, WM_USER_SIMCONNECT, null, 0);
                Console.WriteLine("SimConnect connection established");
                initDataRequest();
                Console.WriteLine("Data Request Initialised");

            }
            catch (COMException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private void SimConnectDisconnect()
        {
            if (simConnectService != null)
            {
                // Dispose serves the same purpose as SimConnect_Close()
                simConnectService.Dispose();
                simConnectService = null;
                Console.WriteLine("Connection closed");
            }
        }

        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == WM_USER_SIMCONNECT)
            {
                if (simConnectService != null)
                {
                    simConnectService.ReceiveMessage();
                }
            }
            else
            {
                base.DefWndProc(ref m);
            }
        }


        // Set up all the SimConnect related data definitions and event handlers
        private void initDataRequest()
        {
            try
            {
                // listen to connect and quit msgs
                simConnectService.OnRecvOpen += new SimConnect.RecvOpenEventHandler(simconnect_OnRecvOpen);
                simConnectService.OnRecvQuit += new SimConnect.RecvQuitEventHandler(simconnect_OnRecvQuit);

                // listen to exceptions
                simConnectService.OnRecvException += new SimConnect.RecvExceptionEventHandler(simconnect_OnRecvException);

                // define a data structure
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "LOCAL TIME", "seconds", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "Title", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "Plane Latitude", "Degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "Plane Longitude", "Degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "PLANE ALTITUDE", "Feet", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "FUEL TOTAL QUANTITY WEIGHT", "Pounds", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "PLANE ALT ABOVE GROUND", "Feet", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "PLANE PITCH DEGREES", "Degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "PLANE BANK DEGREES", "Degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "MAGVAR", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "AIRSPEED TRUE", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "AIRSPEED INDICATED", "knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "VERTICAL SPEED", "feet per minute", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "HEADING INDICATOR", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "G FORCE", "gforce", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:1", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:2", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:3", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:4", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:5", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "GENERAL ENG PCT MAX RPM:6", "rpm", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "ZULU TIME", "seconds", SIMCONNECT_DATATYPE.INT32, 999999999.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "LOCAL TIME", "seconds", SIMCONNECT_DATATYPE.INT32, 999999999.0f, SimConnect.SIMCONNECT_UNUSED);
                simConnectService.AddToDataDefinition(DEFINITIONS.Struct1, "SIM ON GROUND", "bool", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);


                // IMPORTANT: register it with the simconnect managed wrapper marshaller
                // if you skip this step, you will only receive a uint in the .dwData field.
                simConnectService.RegisterDataDefineStruct<Struct1>(DEFINITIONS.Struct1);

                // catch a simobject data request
                simConnectService.OnRecvSimobjectDataBytype += new SimConnect.RecvSimobjectDataBytypeEventHandler(simconnect_OnRecvSimobjectDataBytype);
            }
            catch (COMException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void simconnect_OnRecvSimobjectDataBytype(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {

            switch ((DATA_REQUESTS)data.dwRequestID)
            {
                case DATA_REQUESTS.REQUEST_1:
                    Struct1 s1 = (Struct1)data.dwData[0];


                    bool engines_on = s1.eng1_rpm > 5 || s1.eng2_rpm > 5 || s1.eng3_rpm > 56 || s1.eng4_rpm > 5 || s1.eng5_rpm > 5 || s1.eng6_rpm > 5;
                    string e_on = engines_on ? "1" : "0";

                    string[] parts = new string[]
                    {
                        s1.latitude.ToString(),
                        s1.longitude.ToString(),
                        s1.altitudemsl.ToString(),
                        Math.Floor(s1.heading).ToString(), // TODO - Heading
                        s1.airspeed_indicated.ToString(),
                        s1.ac_pitch.ToString(),
                        s1.ac_bank.ToString(),
                        Math.Floor(s1.vspeed).ToString(), // TODO - Vspeed
                        "0",
                        Math.Round(s1.gforce, 2).ToString(), // TODO - Gforce
                        s1.altitudeagl.ToString(),
                        s1.on_ground.ToString(),
                        s1.paused.ToString(), /// Pause State
                        s1.title.ToString(),
                        "",
                        s1.airspeed_true.ToString(),
                        "",
                        s1.altitudeagl.ToString(),
                        s1.zulu_time.ToString(),
                        e_on,
                        s1.local_time.ToString(),
                        (s1.fuel_total_weight * 0.453592).ToString(),
                    };
                    string payload = $"____loc {string.Join("#", parts)}";
                    Console.Write(payload);
                    Console.Out.Flush();

                    /*Console.WriteLine("##########################");
                    Console.WriteLine("LAT: " + s1.latitude.ToString());
                    Console.WriteLine("LON: " + s1.longitude.ToString());
                    Console.WriteLine("ALTMSL: " + s1.altitudemsl.ToString());
                    Console.WriteLine("ALTAGL: " + s1.altitudeagl.ToString());
                    Console.WriteLine("HEAD: " + s1.heading.ToString()); // TODO - Heading
                    Console.WriteLine("IAS: " + s1.airspeed_indicated.ToString());
                    Console.WriteLine("PITCH: " + s1.ac_pitch.ToString());
                    Console.WriteLine("BANK: " + s1.ac_bank.ToString());
                    Console.WriteLine("VSPEED: " + Math.Floor(s1.vspeed).ToString()); // TODO - Vspeed
                    Console.WriteLine("GFORCE: " + Math.Round(s1.gforce, 2).ToString()); // TODO - Gforce
                    Console.WriteLine("GROUNDED: " + s1.on_ground.ToString());
                    Console.WriteLine("TITLE: " + s1.title.ToString());
                    Console.WriteLine("TAS: " + s1.airspeed_true.ToString());
                    Console.WriteLine("ENGINES ON: " + e_on);
                    Console.WriteLine("E1 RPM: " + s1.eng1_rpm.ToString());
                    Console.WriteLine("E2 RPM: " + s1.eng2_rpm.ToString());
                    Console.WriteLine("E3 RPM: " + s1.eng3_rpm.ToString());
                    Console.WriteLine("E4 RPM: " + s1.eng4_rpm.ToString());
                    Console.WriteLine("E5 RPM: " + s1.eng5_rpm.ToString());
                    Console.WriteLine("E6 RPM: " + s1.eng6_rpm.ToString());
                    Console.WriteLine("ZULU: " + s1.zulu_time.ToString());
                    Console.WriteLine("LOCAL: " + s1.local_time.ToString());
                    Console.WriteLine("FUELWGT: " + s1.fuel_total_weight.ToString());*/


                    Debug.WriteLine(payload);
                    break;

                default:
                    Console.WriteLine("Unknown request ID: " + data.dwRequestID);
                    break;
            }
        }

        void simconnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            Console.WriteLine("Connected to Simulator");
        }

        // The case where the user closes Prepar3D
        void simconnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            Console.WriteLine("Simulator has exited");
            SimConnectDisconnect();
        }

        void simconnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            Console.WriteLine("Exception received: " + data.dwException);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SimConnectDisconnect();
            Application.Exit();
        }

        private void pulse_Tick(object sender, EventArgs e)
        {
            if (simConnectService != null)
            {
                pulse.Interval = 33;
                simConnectService.RequestDataOnSimObjectType(DATA_REQUESTS.REQUEST_1, DEFINITIONS.Struct1, 0, SIMCONNECT_SIMOBJECT_TYPE.USER);
            } else
            {
                pulse.Interval = 2500;
                SimConnectConnect();
            }

        }
    }
}
